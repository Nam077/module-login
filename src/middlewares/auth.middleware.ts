import { NextFunction, Response } from 'express';
import { verify } from 'jsonwebtoken';
import { SECRET_KEY } from '@config';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, RequestWithUser } from '@interfaces/auth.interface';
import userModel, { User } from '@models/users.model';

const authMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
        const Authorization =
            req.cookies['Authorization'] ||
            (req.header('Authorization') ? req.header('Authorization').split('Bearer ')[1] : null);

        if (Authorization) {
            const secretKey: string = SECRET_KEY;
            const verificationResponse = (await verify(Authorization, secretKey)) as DataStoredInToken;
            const userId = verificationResponse._id;
            const findUser = await userModel.findById(userId);

            if (findUser) {
                req.user = findUser;
                next(findUser);
            } else {
                next(new HttpException(401, 'Wrong authentication token'));
            }
        } else {
            next(new HttpException(404, 'Authentication token missing'));
        }
    } catch (error) {
        next(new HttpException(401, 'Wrong authentication token'));
    }
};
export const adminMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction, findUser: User) => {
    try {
        if (findUser.role === 'admin') {
            next();
        } else {
            next(new HttpException(401, 'You are not an admin'));
        }
    } catch (error) {
        next(new HttpException(401, 'You are not an admin'));
    }
};

export default authMiddleware;
