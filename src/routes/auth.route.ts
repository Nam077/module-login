import { Router } from 'express';
import AuthController from '@controllers/auth.controller';
import { CreateUserDto, ForgotPasswordDto, ResetPasswordDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from '@middlewares/auth.middleware';
import validationMiddleware from '@middlewares/validation.middleware';

class AuthRoute implements Routes {
    public path = '/';
    public router = Router();
    public authController = new AuthController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}sign-in`, this.authController.signIn);
        this.router.post(`${this.path}signup`, validationMiddleware(CreateUserDto, 'body'), this.authController.signUp);
        this.router.post(`${this.path}login`, validationMiddleware(CreateUserDto, 'body'), this.authController.logIn);
        this.router.post(`${this.path}logout`, authMiddleware, this.authController.logOut);
        this.router.post(
            `${this.path}forgot-password`,
            validationMiddleware(ForgotPasswordDto, 'body'),
            this.authController.forgotPassword,
        );
        this.router.post(
            `${this.path}reset-password`,
            validationMiddleware(ResetPasswordDto, 'body'),
            this.authController.resetPassword,
        );
        this.router.post(`${this.path}change-password`, authMiddleware, this.authController.changePassword);
        this.router.get(`${this.path}google`, this.authController.loginWithGoogle);
        this.router.get(`${this.path}google/callback`, this.authController.loginWithGoogleCallback);
        this.router.get(`${this.path}github`, this.authController.loginWithGithub);
        this.router.get(`${this.path}github/callback`, this.authController.loginWihGitHUbCallback);
    }
}
export default AuthRoute;
