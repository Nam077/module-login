import { Router } from 'express';
import UsersController from '@controllers/users.controller';
import { CreateUserDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import authMiddleware from '@middlewares/auth.middleware';
import adminMiddleware from '@middlewares/auth.middleware';

class UsersRoute implements Routes {
    public path = '/admin/users';
    public router = Router();
    public usersController = new UsersController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, authMiddleware, adminMiddleware, this.usersController.getUsers);
        this.router.get(`${this.path}/:id`, authMiddleware, adminMiddleware, this.usersController.getUserById);
        this.router.post(
            `${this.path}`,
            authMiddleware,
            adminMiddleware,
            validationMiddleware(CreateUserDto, 'body'),
            this.usersController.createUser,
        );
        this.router.put(
            `${this.path}/:id`,
            authMiddleware,
            adminMiddleware,
            validationMiddleware(CreateUserDto, 'body', true),
            this.usersController.updateUser,
        );
        this.router.delete(`${this.path}/:id`, authMiddleware, adminMiddleware, this.usersController.deleteUser);
    }
}

export default UsersRoute;
