import { NextFunction, Request, Response } from 'express';
import { ChangePasswordDto, CreateUserDto, ResetPasswordDto } from '@dtos/users.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import AuthService from '@services/auth.service';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@models/users.model';
import passport from 'passport';

class AuthController {
    public authService = new AuthService();

    public signUp = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userData: CreateUserDto = req.body;
            const signUpUserData: User = await this.authService.signup(userData);

            res.status(201).json({ data: signUpUserData, message: 'signup' });
        } catch (error) {
            next(error);
        }
    };

    public logIn = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userData: CreateUserDto = req.body;
            const { cookie, findUser } = await this.authService.login(userData);

            res.setHeader('Set-Cookie', [cookie]);
            res.status(200).json({ data: findUser, message: 'login' });
        } catch (error) {
            next(error);
        }
    };

    public logOut = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        try {
            const userData: User = req.user;
            const logOutUserData: User = await this.authService.logout(userData);

            res.setHeader('Set-Cookie', ['Authorization=; Max-age=0']);
            res.status(200).json({ data: logOutUserData, message: 'logout' });
        } catch (error) {
            next(error);
        }
    };
    public forgotPassword = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data: CreateUserDto = req.body;
            const dataForgotPassword = await this.authService.forgotPassword(data);
            res.status(200).json({ data: dataForgotPassword, message: 'forgotPassword' });
        } catch (error) {
            next(error);
        }
    };
    public resetPassword = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data: ResetPasswordDto = req.body;
            const token = req.params.token;
            const dataResetPassword = await this.authService.resetPassword(data, token);
            res.status(200).json({ data: dataResetPassword, message: 'resetPassword' });
        } catch (error) {
            next(error);
        }
    };
    public changePassword = async (findUser: User, req: RequestWithUser, res: Response, next: NextFunction) => {
        try {
            const data: ChangePasswordDto = req.body;
            const id = findUser._id;
            const dataChangePassword = await this.authService.changePassword(data, id);
            res.status(200).json({ data: dataChangePassword, message: 'changePassword' });
        } catch (error) {
            next(error);
        }
    };
    public signIn(req: Request, res: Response, next: NextFunction) {
        try {
            return res.render('login');
        } catch (error) {
            next(error);
        }
    }
    public async loginWithGithub(userData: CreateUserDto) {
        const data = passport.authenticate('github', { scope: ['profile', 'email'] });
    }
    public async loginWihGitHUbCallback(userData: CreateUserDto) {
        passport.authenticate('github', { failureRedirect: '/login' });
    }
    public async loginWithGoogle(userData: CreateUserDto) {
        passport.authenticate('google', { scope: ['profile', 'email'] });
    }
    public async loginWithGoogleCallback(userData: CreateUserDto) {
        passport.authenticate('google', { failureRedirect: '/login' });
    }
}

export default AuthController;
